from plant import Plant

class Alumno():

    def __init__(self, nombre):
        self._nombre = nombre
        self._plantas_fila1 = []
        self._plantas_fila2 = []
        pass

    @property
    def nombre(self):
        return self._nombre
    @nombre.setter
    def nombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")

    @property
    def plantas_fila1(self):
        return self._plantas_fila1
    @plantas_fila1.setter
    def plantas_fila1(self, plantas):
        if isinstance(plantas, Plant):
            self._plantas_fila1.append(plantas)
        else:
            print("El tipo de dato no corresponde")
    
    @property
    def plantas_fila2(self):
        return self._plantas_fila2
    @plantas_fila2.setter
    def plantas_fila2(self, plantas):
        if isinstance(plantas, Plant):
            self._plantas_fila2.append(plantas)
        else:
            print("El tipo de dato no corresponde")
