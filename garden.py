import random
from alumno import Alumno
from plant import Plant

semillas = [Plant("H"), Plant("T"),
            Plant("R"), Plant("V")]


class Garden():

    def __init__(self, nombre):
        self._nombre = nombre
        self._alumnos = []
        pass

    def llegan_alumnos(self):
        #Se añaden los objetos alumnos al objeto garde
        self._alumnos = [Alumno("Alicia"), Alumno("Andres"), 
                         Alumno("Belen"), Alumno("David"), 
                         Alumno("Eva"), Alumno("Jose"),
                         Alumno("Larry"), Alumno("Lucia"),
                         Alumno("Marit"), Alumno("Pepito"),
                         Alumno("Rocio"), Alumno("Sergio")]

    def poner_vasitos(self):
        #Aqui se entregan los vasitos con sus semillas a cada alumno de manera aleatoria
        for i in self._alumnos:
            for j in range(2):
                i.plantas_fila1 = random.choice(semillas)
                i.plantas_fila2 = random.choice(semillas)
    
    def mostrar_ventana(self):
        #Este metodo sirve para ver todos los vasitos con semillas en la ventana
        print("      PLANTAS KINDER B")
        print("[ventana][ventana][ventana]")

        for i in self._alumnos:
            for j in i.plantas_fila1:   
                print(j.nombre, end="")
        print("")
        for i in self._alumnos:    
            for j in i.plantas_fila2:
                print(j.nombre, end="")
        print("")

    def plantas(self):
        #Este metodo permite imprimir de manera especifica segun el alumno en interes
        decision = None
        while decision != "salir":
            print("Alicia" )
            print("Andres" )  
            print("Belen" )  
            print("David" )      
            print("Eva" )  
            print("Jose" )
            print("Larry" )
            print("Lucia" )
            print("Marit" )
            print("Pepito" )
            print("Rocio" ) 
            print("Sergio" )
            print("Salir")
            try: 
                decision = str(input("¿Que plantas quiere ver?"))
            except ValueError:
                print("Ingrese un dato valido")

            decision = decision.lower()

            #Mediante estos if se recorren todas las plantas de los alumnos y solo se imprime el de interes
            if decision == "alicia":
                print("      PLANTAS DE ALICIA")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Alicia":
                           print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Alicia":
                           print(j.nombre, end="")
                        else:
                            print("-", end="")        
                print("")    
                     
            elif decision == "andres":
                print("      PLANTAS DE ANDRES")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Andres":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Andres":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")    
                print("")

            elif decision == "belen":
                print("      PLANTAS DE BELEN")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Belen":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Belen":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                    
                print("")

            elif decision == "david":
                print("      PLANTAS DE DAVID")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "David":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "David":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "eva":
                print("      PLANTAS DE EVA")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Eva":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Eva":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "jose":
                print("      PLANTAS DE JOSE")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Jose":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Jose":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "larry":
                print("      PLANTAS DE LARRY")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Larry":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Larry":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "lucia":
                print("      PLANTAS DE LUCIA")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Lucia":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Lucia":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "marit":
                print("      PLANTAS DE MARIT")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Marit":
                            print(j.nombre, end="")
                        else:
                            print("-", end="") 
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Marit":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "pepito":
                print("      PLANTAS DE PEPITO")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Pepito":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Pepito":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "rocio":
                print("      PLANTAS DE ROCIO")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Rocio":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Rocio":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "sergio":
                print("      PLANTAS DE SERGIO")
                print("[ventana][ventana][ventana]")
                for i in self._alumnos:
                    for j in i.plantas_fila1:
                        if i.nombre == "Sergio":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")
                for i in self._alumnos:
                    for j in i.plantas_fila2:
                        if i.nombre == "Sergio":
                            print(j.nombre, end="")
                        else:
                            print("-", end="")
                print("")

            elif decision == "salir":
                print("Hasta luego")
            
            else:
                print("INGRESE UN DATO VALIDO")

garden = Garden("Kinder B")
garden.llegan_alumnos()
garden.poner_vasitos()
garden.mostrar_ventana()    
garden.plantas()
